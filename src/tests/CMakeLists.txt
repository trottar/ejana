cmake_minimum_required(VERSION 3.3)
project(test_ejana)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
        tests.cc
        ejana/test_StringHelpers_LexicalSplit.cc
        ejana/test_StringHelpers_Various.cc
        )

add_executable(${PROJECT_NAME} ${SOURCE_FILES})

target_include_directories(${PROJECT_NAME}
        PRIVATE
        ${PROJECT_SOURCE_DIR}
        )
target_link_libraries(${PROJECT_NAME} -L${JANA_LIB_DIR} ${JANA_LIB} dl)


