
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class FairMCPoint+;
#pragma link C++ class FairMCEventHeader+;
#pragma link C++ class FairGenericStack+;
#pragma link C++ class FairGenerator+;
#pragma link C++ class FairPrimaryGenerator+;

#endif
