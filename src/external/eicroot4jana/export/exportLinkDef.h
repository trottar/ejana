
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EicRcEvent+;
#pragma link C++ class EicRcParticle+;
#pragma link C++ class EicRcCalorimeterHit+;

//#pragma link C++ class EicRcVertex+;
//#pragma link C++ class EicEventAssembler+;
//#pragma link C++ class EicCalorimeterHub+;
//#pragma link C++ class EicRootManager+;
//#pragma link C++ class EicRootInputFile+;

#endif
