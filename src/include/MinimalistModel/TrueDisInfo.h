#ifndef EJANA_TRUEDISINFO_H
#define EJANA_TRUEDISINFO_H

#include <JANA/JObject.h>

namespace minimodel {
    class TrueDisInfo: public JObject {
    public:
        double x;
        double q2;
        double y;
        double w2;
        double nu;
        double t_hat;
    };
}

#endif //EJANA_TRUEDISINFO_H
