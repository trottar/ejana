#ifndef EJANA_MAINOUTPUTROOTFILE_H
#define EJANA_MAINOUTPUTROOTFILE_H

#include <TFile.h>
#include <JANA/Services/JServiceLocator.h>

/**
 *  This class allows ROOT TFile to be used by JANA service locator
 *
 */
namespace ej
{
    class MainOutputRootFile: public TFile, public JService
    {
    public:
        MainOutputRootFile(const char *fname, Option_t *option="", const char *ftitle="", Int_t compress = ROOT::RCompressionSetting::EDefaults::kUseCompiledDefault):
                TFile(fname, option, ftitle, compress)
        {
        }

        MainOutputRootFile(const MainOutputRootFile &) = delete;            //Files cannot be copied
        void operator=(const TFile &) = delete;
    };
}



#endif //EJANA_MAINOUTPUTROOTFILE_H
