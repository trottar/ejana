//
// Created by romanov on 5/6/19.
//

#ifndef EJANA_HepMCEventData_H
#define EJANA_HepMCEventData_H

#include "JANA/JObject.h"
#include "HepMC/GenEvent.h"
#include <MinimalistModel/McGeneratedVertex.h>

class JApplication;



namespace ej
{
    class HepMCEventData: public JObject
    {

    public:
        explicit HepMCEventData(HepMC::GenEvent *event):
                hepmc_event(event)
        {
        }

        std::shared_ptr<HepMC::GenEvent> hepmc_event;
    };
}


#endif //EJANA_HepMCEventData_H
