// $Id$
//
//    File: SampleAnalysis.cc
// Created: Mon Oct 23 22:38:48 EDT 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#include <iostream>
#include <algorithm>

#include <TDirectory.h>
#include <TCanvas.h>
#include <TPad.h>

#include <JANA/JApplication.h>
#include <JANA/JEvent.h>

#include <fmt/core.h>


#include <ejana/EServicePool.h>

#include "DummyEventSource.h"

#include "MultiThreadingAnalysis.h"


using namespace fmt;

//------------------
// SampleAnalysis (Constructor)
//------------------
MultiThreadingAnalysis::MultiThreadingAnalysis(JApplication *app) :
	JEventProcessor(app),
	services(app)
{
}

//------------------
// Init
//------------------
void MultiThreadingAnalysis::Init()
{
	// Ask service locator a file to write to
	auto file = services.Get<TFile>();

	// Root related, we switch gDirectory to this file
	file->cd();
	gDirectory->pwd();

	// Create a directory for this plugin. And subdirectories for series of histograms
	dir_main = file->mkdir("root_mt_bench");

	hist_pt = new TH1D("pt", "particle pt", 1000, 0, 50);
}


//------------------
// Process
//------------------
void MultiThreadingAnalysis::Process(const std::shared_ptr<const JEvent>& event)
{
    fmt::print("thread={}\n", std::this_thread::get_id());
    auto data = event->GetSingle<DummyEventDataOther>();
}


//------------------
// Finish
//------------------
void MultiThreadingAnalysis::Finish()
{

}