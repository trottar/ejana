// $Id$
//
//    File: SampleAnalysis.cc
// Created: Mon Oct 23 22:38:48 EDT 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#include <iostream>
#include <algorithm>

#include <TDirectory.h>
#include <TCanvas.h>
#include <TPad.h>

#include <JANA/JApplication.h>
#include <JANA/JEvent.h>

#include <fmt/core.h>

#include <ejana/EStringHelpers.h>
#include <ejana/EServicePool.h>

#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McFluxHit.h>

#include "SampleAnalysis.h"


using namespace fmt;

//------------------
// SampleAnalysis (Constructor)
//------------------
SampleAnalysis::SampleAnalysis(JApplication *app) :
	JEventProcessor(app),
	services(app)
{
}

//------------------
// Init
//------------------
void SampleAnalysis::Init()
{
	// Ask service locator a file to write to
	auto file = services.Get<TFile>();

	// Root related, we switch gDirectory to this file
	file->cd();
	gDirectory->pwd();

	// Create a directory for this plugin. And subdirectories for series of histograms
	dir_main = file->mkdir("sample_ana");

	hist_pt = new TH1F("pt", "particle pt", 100, 0, 50);
}


//------------------
// Process
//------------------
void SampleAnalysis::Process(const std::shared_ptr<const JEvent>& event)
{
    fmt::print("SampleAnalysis::Process() called. Event number: {}\n", event->GetEventNumber());

	auto particles = event->Get<minimodel::McGeneratedParticle>();

 	for(auto partice: particles) {
 	    if(partice->pdg == 2212) {
 	        double pt = sqrt(partice->px*partice->px + partice->py*partice->py);
 	        hist_pt->Fill(pt);
 	    }
	}
}


//------------------
// Finish
//------------------
void SampleAnalysis::Finish()
{
	fmt::print("SampleAnalysis::Finish() called\n");
}


