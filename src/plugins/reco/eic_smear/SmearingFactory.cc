#include "SmearingFactory.h"

// TODO organize includes
#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>
#include "eicsmear/erhic/VirtualParticle.h"
#include "eicsmear/smear/Acceptance.h"
#include "eicsmear/smear/Device.h"
#include "eicsmear/smear/Detector.h"
#include "eicsmear/smear/Smearer.h"
#include "eicsmear/smear/ParticleMCS.h"
#include "eicsmear/smear/PerfectID.h"
#include <eicsmear/smear/Smear.h>
#include <eicsmear/erhic/ParticleMC.h>
#include "Math/Vector4D.h"
#include <ejana/EServicePool.h>
#include <MinimalistModel/McGeneratedParticle.h>

#include <TClingRuntime.h>

#include <fmt/format.h>     // For format and print functions
#include <cstdlib>
#include <cmath>
#include <mutex>

#include "ZeusDetector.h"
#include "BeASTDetector.h"
#include "ePHENIXDetector.h"
#include "JleicSmear.h"

void SmearingFactory::Init() {

    _beast_detector = BuildBeAST();
    _zeus_detector = BuildZeus();
    _ephenix_detector = BuildEphenix();

    //jleic_eic_smear_detector = BuildJLEIC();
    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    _pm->SetDefaultParameter("eic_smear:verbose", _verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");
    _pm->SetDefaultParameter("eic_smear:detector", _detector_name, "Detector name: beast, jleic, zeus");
    _pm->SetDefaultParameter("eic_smear:throw", _throw_not_smeared, "Throw away not smeared particles");
    _pm->SetDefaultParameter("eic_smear:strict", _strict_particle_selection,
                             "False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared");
/*
 *
    Each particle has smearing info with flags
    has_e;           /// Energy was smeared
    has_p;           /// Momentum was smeared
    has_pid;         /// PID is smeared
    has_vtx;         /// Vertex is smeared
    has_any_eppid;   /// Has smeared e, p, or pid
    There are two eic_smear plugin flags
    eic_smear:throw - Throw away not smeared particles
    eic_smear:strict - False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared




    .plugin('jana', output='beagle.root', nthreads=10)\
    .source('../data/beagle_eD.txt')



  */
}

void SmearingFactory::SmearWithEicSmear(minimodel::McGeneratedParticle *particle) {

    // EIC smear
    erhic::ParticleMC not_smeared_prt;
    not_smeared_prt.SetId(particle->pdg);           // PDG particle code
    not_smeared_prt.SetIndex(particle->id);         // Particle index in event
    not_smeared_prt.SetStatus(particle->gen_code);  // Particle status code: like in PYTHIA, Beagle, etc
    not_smeared_prt.Set4Vector(TLorentzVector(particle->px, particle->py, particle->pz, particle->tot_e));
    not_smeared_prt.SetP(particle->p);

    // input p and e
    double in_p = particle->p;
    double in_e = particle->tot_e;

    Smear::ParticleMCS * smeared_prt;
    if(_detector_name == "beast") {
        smeared_prt = _beast_detector.Smear(not_smeared_prt);
    }
    else if (_detector_name == "ephenix") {
        smeared_prt = _ephenix_detector.Smear(not_smeared_prt);
    }
    else {
        smeared_prt = _zeus_detector.Smear(not_smeared_prt);
    }

    if(!smeared_prt) {
        _stat.null_particles++;
        return;
    }

    // fmt::print("P {:<10} {:<10}  E {:<10}  {:<10}\n", source_particle->p, smeared_prt->GetP(), source_particle->tot_e, smeared_prt->GetE());

    // Copy back the particle
    particle->px = smeared_prt->GetPx();
    particle->py = smeared_prt->GetPy();
    particle->pz = smeared_prt->GetPz();
    particle->tot_e = smeared_prt->GetE();

    // Check odds of eic-smear smearing
    double sm_p = smeared_prt->GetP();
    double sm_e = smeared_prt->GetE();

    // Eic smear return non zero p
    bool zero_p = TMath::Abs(in_p)>0.00001 && TMath::Abs(sm_p)<0.00001;
    bool zero_e = TMath::Abs(in_e)>0.00001 && TMath::Abs(sm_e)<0.00001;

    if(zero_p && zero_e) {
        _stat.zero_e_zero_p ++;
        particle->smear.has_any_eppid = false;
    } else if (zero_p) {
        _stat.smear_e_zero_p++;
        particle->smear.has_any_eppid = true;
        particle->smear.has_e = true;
    } else if (zero_e) {
        _stat.zero_e_smear_p++;
        particle->smear.has_any_eppid = true;
        particle->smear.has_p = true;
    } else {
        _stat.smear_e_smear_p++;
        particle->smear.has_any_eppid = true;
        particle->smear.has_p = true;
        particle->smear.has_e = true;
    }
}

void SmearingFactory::Process(const std::shared_ptr<const JEvent> &event) {
    using namespace fmt;
    using namespace std;

    // Get the inputs needed for this factory.
    auto source_particles = event->Get<minimodel::McGeneratedParticle>();

    vector<minimodel::McGeneratedParticle*> dest_particles;

    for (auto source_particle : source_particles) {
        auto dest_particle = new minimodel::McGeneratedParticle();
        *dest_particle = *source_particle;      // Copy all fields of source particle to dest particle

        dest_particle->has_smear_info = false;  // We don't know if particle will be smeared at all
        dest_particle->smear.orig_tot_e = dest_particle->tot_e;   /// original Energy
        dest_particle->smear.orig_p     = dest_particle->p;       /// original total momentum
        dest_particle->smear.orig_px    = dest_particle->px;      /// original px
        dest_particle->smear.orig_py    = dest_particle->py;      /// original py
        dest_particle->smear.orig_pz    = dest_particle->pz;      /// original pz
        dest_particle->smear.orig_vtx_x = dest_particle->vtx_x;   /// original px
        dest_particle->smear.orig_vtx_y = dest_particle->vtx_y;   /// original py
        dest_particle->smear.orig_vtx_z = dest_particle->vtx_z;   /// original pz

        _stat.total_particles++;

        if (_detector_name == "beast" ||
            _detector_name == "zeus"  ||
            _detector_name == "ephenix") {

            // EIC-Smear is used here:
            SmearWithEicSmear(dest_particle);
        } else {

            // It is jleic?
            if (_detector_name != "jleic") {
                PrintUnknownDetectorWarning();
            }

            SmearPrticleJleic(dest_particle, _verbose);

            // jleic smearing stats
            if(dest_particle->smear.has_e && dest_particle->smear.has_p) {
                _stat.smear_e_smear_p++;
            } else if (dest_particle->smear.has_e && !dest_particle->smear.has_p) {
                _stat.smear_e_zero_p++;
            } else if (!dest_particle->smear.has_e && dest_particle->smear.has_p) {
                _stat.zero_e_smear_p++;
            } else {
                _stat.zero_e_zero_p ++;
            }
        }

        if (_verbose>=2) {
            fmt::print("pdg {:<7}  P in {:<10} {:<10}  E in {:<10}  {:<10}\n",
                       source_particle->pdg,
                       source_particle->p, dest_particle->p,
                       source_particle->tot_e, dest_particle->tot_e);
        }

        // Set new full momentum parameter
        dest_particle->p = sqrt(dest_particle->px*dest_particle->px +
                                  dest_particle->py*dest_particle->py +
                                  dest_particle->pz*dest_particle->pz);

        // Adding a particle to the result list
        if(!_throw_not_smeared) {
            dest_particles.push_back(dest_particle);
        } else {
            if(_strict_particle_selection && dest_particle->smear.has_p && dest_particle->smear.has_e) {
                dest_particles.push_back(dest_particle);
            }
            if(!_strict_particle_selection && dest_particle->smear.has_any_eppid) {
                dest_particles.push_back(dest_particle);
            }
        }
    }

    Set(std::move(dest_particles));
}

void SmearingFactory::PrintSmearStats() {
    fmt::print("SmearingFactory statistics:\n");
    fmt::print("   total_particles = {:<10} \n", _stat.total_particles);
    fmt::print("   null_particles  = {:<10} (not smeared)\n", _stat.null_particles);
    fmt::print("   zero_e_zero_p   = {:<10} (not smeared)\n", _stat.zero_e_zero_p);
    fmt::print("   zero_e_smear_p  = {:<10} (partly smeared)\n", _stat.zero_e_smear_p);
    fmt::print("   smear_e_zero_p  = {:<10} (partly smeared)\n", _stat.smear_e_zero_p);
    fmt::print("   smear_e_smear_p = {:<10} (smeared)\n", _stat.smear_e_smear_p);
}

void SmearingFactory::PrintUnknownDetectorWarning() {
    // Unknown detector is used. print warning
    std::call_once(WarnNoNameWarningOnceFlag(), [this](){
        fmt::print("eic_smear: (!) WARNING detector name = '{}' is not set or not known. \n"
              "Set 'eic_smear:detector' parameter to 'jleic', 'beast', 'ephenix' or 'zeus'. This={}\n"
              "'jleic' detector is being used as default\n", this->_detector_name, (size_t)(void*)this);
    });
}

