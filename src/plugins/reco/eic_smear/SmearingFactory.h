//
// Created by romanov on 7/11/2019.
//

#ifndef EJANA_SMEARINGFACTORY_H
#define EJANA_SMEARINGFACTORY_H
#include <mutex>

#include <JANA/JFactoryT.h>
#include <JANA/Services/JParameterManager.h>
#include <JANA/Utils/JTypeInfo.h>

#include <MinimalistModel/McGeneratedParticle.h>
#include <eicsmear/smear/Detector.h>


class SmearingFactory: public JFactoryT<minimodel::McGeneratedParticle> {
public:

    explicit SmearingFactory(JParameterManager *pm, const std::string& aTag = "smear") :
        JFactoryT<minimodel::McGeneratedParticle>(JTypeInfo::demangle<SmearingFactory>(), aTag)
    {
        this->_pm = pm;
    }

    void PrintSmearStats();

    virtual ~SmearingFactory() {
        PrintSmearStats();
    }

    void Init() override;
    void Process(const std::shared_ptr<const JEvent>& event) override;

protected:
    void SmearPrticleJLEIC(minimodel::McGeneratedParticle *particle);
    void SmearWithEicSmear(minimodel::McGeneratedParticle *particle);

private:
    std::once_flag& WarnNoNameWarningOnceFlag() {
        static std::once_flag _warn_no_name_provided_once_flag;  // This is used to tell once, that detector name is not given
                                                                 // and smearing is not performed
        return _warn_no_name_provided_once_flag;
    }

    Smear::Detector _beast_detector;
    Smear::Detector _zeus_detector;
    Smear::Detector _ephenix_detector;

    int _verbose = 0;                          // verbose output level 0 - none, 1 - info, 2 - all
    bool _throw_not_smeared = true;            // throw particles which are not smeared
    bool _strict_particle_selection = false;   // False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared
    std::string _detector_name = "";           // detector name to smear
    JParameterManager *_pm;

    // Some statistics about the smearing process
    struct EicSmearStatistics {
        uint64_t total_particles = 0;
        uint64_t null_particles = 0;
        uint64_t zero_e_smear_p = 0;
        uint64_t smear_e_zero_p = 0;
        uint64_t smear_e_smear_p = 0;
        uint64_t zero_e_zero_p = 0;
    } _stat;

    void PrintUnknownDetectorWarning();
};

extern std::once_flag a;
#endif //EJANA_SMEARINGFACTORY_H
