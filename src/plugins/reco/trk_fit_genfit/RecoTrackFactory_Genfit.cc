
#include "RecoTrackFactory_Genfit.h"

#include <JANA/JEvent.h>

void RecoTrackFactory_Genfit::Init() {

    auto app = GetApplication();
    app->SetDefaultParameter("trk_fit_genfit:use_truth_for_estimate", use_truth_for_estimate);
    app->SetDefaultParameter("trk_fit_genfit:interactive", interactive);
}

void RecoTrackFactory_Genfit::ChangeRun(const std::shared_ptr<const JEvent> &event) {}

void RecoTrackFactory_Genfit::Process(const std::shared_ptr<const JEvent> &event) {

    EicGenfitInput input;
    input.clusters = event->Get<RecoCluster>();
    input.interactive = interactive;

    if (use_truth_for_estimate) {
        input.truths = event->Get<minimodel::McTrack>();
    }

    genfit_results = FitTracks(input);

    Set(genfit_results.tracks);
    SetMetadata(genfit_results.metadata);
}
