#include <JANA/JFactoryGenerator.h>
#include <JANA/JApplication.h>
#include <JANA/JFactoryT.h>

#include "RecoModel/RecoHit.h"
#include "RecoModel/RecoCluster.h"

class JFactoryGenerator_vtx_fit:public JFactoryGenerator{
public:

    void GenerateFactories(JFactorySet *factory_set) override {

        factory_set->Add(new JFactoryT<RecoHit>());
        factory_set->Add(new JFactoryT<RecoCluster>());
    }
};


// Routine used to create our JEventProcessor
extern "C"{
    void InitPlugin(JApplication *app){
        InitJANAPlugin(app);
        app->Add(new JFactoryGenerator_vtx_fit());
    }
} // "C"
