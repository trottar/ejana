/data/yulia/Herwig6/herwig6_P19104_Q10_N5e+06_e-p_5x100.hepmc

## Clion configuration
Target: jleic_occupancy
Executable: ejana

Program arguments:
```
-Pplugins=g4e_reader,jleic_occupancy
/home/romanov/eic/data/g4e_p_cone_100ev_2019-09.root
```

Working directory:
```
$PROJECT_DIR$/cmake-build-debug
```

Environment variables:
```
JANA_PLUGIN_PATH=$PROJECT_DIR$/cmake-build-debug
```

## Advanced plugin params:

```bash
-Pplugins=hepmc_reader,open_charm
-Popen_charm:verbose=2
-Popen_charm:smearing=1
-Popen_charm:e_beam_energy=5
-Popen_charm:ion_beam_energy=100
-Pjana:DEBUG_PLUGIN_LOADING=1
-Pnevents=5000
-CCCPnthreads=1
-PROOT:StartGui=1
/data/yulia/Herwig6/herwig6_P19104_Q10_N5e+06_e-p_5x100.hepmc
```