// $Id$
//
//    File: OccupancyAnalysis.cc
// Created: Mon Oct 23 22:38:48 EDT 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#include <iostream>
#include <algorithm>

#include <TDirectory.h>
#include <TCanvas.h>
#include <TPad.h>

#include <JANA/JApplication.h>
#include <JANA/JEvent.h>

#include <fmt/core.h>

#include <ejana/EStringHelpers.h>
#include <ejana/EServicePool.h>

#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McFluxHit.h>

#include "OccupancyAnalysis.h"


using namespace fmt;

//------------------
// OccupancyAnalysis (Constructor)
//------------------
OccupancyAnalysis::OccupancyAnalysis(JApplication *app) :
	JEventProcessor(app),
	services(app)
{
}

//------------------
// Init
//------------------
void OccupancyAnalysis::Init()
{
	// Ask service locator a file to write to
	auto file = services.Get<TFile>();

	// Root related, we switch gDirectory to this file
	file->cd();
	fmt::print("OccupancyAnalysis::gDirectory->pwd()\n");	// >oO Debug print
	gDirectory->pwd();

	// Create a directory for this plugin. And subdirectories for series of histograms
	dir_main = file->mkdir("occupancy_ana");
	dir_th2_by_layer = dir_main->mkdir("th2_by_layer");
	dir_th2_by_detector = dir_main->mkdir("th2_by_detector");
	dir_th3_by_detector = dir_main->mkdir("th3_by_detector");
	dir_th1_by_detector = dir_main->mkdir("th1_by_detector");

	// CREATE HISTOGRAMS:

	// Hits by Z distribution
	th1_hits_z = new TH1D("hits_z", "Hits Z distribution [mm]", 851, -2000, 6500);
	th1_hits_z->SetDirectory(dir_main);

	// Total xy occupancy
	total_occ = new TH2F(NAME_OF(total_occ), "Total XY occupancy (all Z)", 600, -2000, 2000, 300, -2000., 2000.);
	total_occ->SetDirectory(dir_main);
	total_occ->SetOption("COLSCATZ");		// Draw as heat map by default

	// Hadron part xy occupancy (z > 0)
	h_part_occ = new TH2F(NAME_OF(h_part_occ), "Hadron part xy occupancy (z > 0)", 600, -2000, 2000, 300, -2000., 2000.);
	h_part_occ->SetDirectory(dir_main);
	h_part_occ->SetOption("COLSCATZ");		// Draw as heat map by default

	// electron part xy occupancy (Z < 0)
	e_part_occ = new TH2F(NAME_OF(e_part_occ), "Electron part xy occupancy (Z < 0)", 600, -2000, 2000, 300, -2000., 2000.);
	e_part_occ->SetDirectory(dir_main);
	e_part_occ->SetOption("COLSCATZ");		// Draw as heat map by default


	th3_hits3d = new TH3F("hits_3d", "JLEIC Hits;Z (mm);X (mm); Y (mm)",
						  200, -2500.0, 2500.0,  // Z NBins, min, max
						  200, -1500.0, 1500.0,  // X NBins, min, max
						  200, -1500.0, 1500.0); // Y NBins, min, max
	th3_hits3d->SetDirectory(dir_main);


	// 3d histograms by detector. In lambda we tell hot to create the histogram
	th3_by_detector = new PtrByNameMapHelper<TH3F>(
			[this](const std::string &name) {
				std::lock_guard<std::recursive_mutex> locker(lock);   // Make this multi-threading safe

				auto hist_name = name + "_th3bd";
				auto hist = new TH3F(hist_name.c_str(), "JLEIC Hits;Z (mm);X (mm); Y (mm)",
									 200, -2500.0, 2500.0,  // Z NBins, min, max
									 200, -1500.0, 1500.0,  // X NBins, min, max
									 200, -1500.0, 1500.0); // Y NBins, min, max
				hist->SetDirectory(this->dir_th3_by_detector);
				return hist;
			});

	// 2d histograms by detector
	th2_by_detector = new PtrByNameMapHelper<TH2F>(
			[this](const std::string &name) {
				std::lock_guard<std::recursive_mutex> locker(lock);   // Make this multi-threading safe

				auto roo_name = name + "_th2bd";
				auto title = fmt::format("Occupancy of '{}' detector", name);
				auto hist = new TH2F(roo_name.c_str(), title.c_str(), 600, -2000, 2000, 300, -3000., 3000.);
				hist->SetOption("COLSCATZ");	// Draw with colors
				hist->SetDirectory(this->dir_th2_by_detector);
				return hist;
			});

	// 2d histograms by each individual detector layer
	th2_by_layer = new PtrByNameMapHelper<TH2F>(
			[this](const std::string &name) {
				std::lock_guard<std::recursive_mutex> locker(lock);   // Make this multi-threading safe

				auto roo_name = name + "_th3bl";
				auto title = fmt::format("Occupancy of '{}' layer", name);
				auto hist = new TH2F(roo_name.c_str(), title.c_str(), 600, -2000, 2000, 300, -3000., 3000.);
				hist->SetDirectory(this->dir_th2_by_layer);
				return hist;
			});

	th1_z_by_detector = new PtrByNameMapHelper<TH1F>(
			[this](const std::string &name) {
				std::lock_guard<std::recursive_mutex> locker(lock);   // Make this multi-threading safe

				auto roo_name = name + "_th1zbd";
				auto title = fmt::format("Hits Z distribution by '{}' detector", name);
				auto hist = new TH1F(roo_name.c_str(), title.c_str(), 851, -2000, 6500);
				hist->SetDirectory(this->dir_th1_by_detector);
				return hist;
			});


	// Compton and tagger
	// Set colors for detectors
	th3_by_detector->Get(detector_names[0])->SetMarkerColor(kRed+3);
	th3_by_detector->Get(detector_names[1])->SetMarkerColor(kBlue+2);
	th3_by_detector->Get(detector_names[2])->SetMarkerColor(kGreen+1);
}


//------------------
// Process
//------------------
void OccupancyAnalysis::Process(const std::shared_ptr<const JEvent>& event)
{
	// Get hits
	auto hits = event->Get<minimodel::McFluxHit>();

 	for(auto hit: hits) {

		// Create local x,y,z,name as we will use them a lot to drag hit-> around
		double x = hit->x;
		double y = hit->y;
		double z = hit->z;

		th1_hits_z->Fill(z);	// Hits over z axes
		total_occ->Fill(x, y);	// Total xy occupancy
		if(z > 0) h_part_occ->Fill(x, y);	// Hadron part xy occupancy (z > 0)
		if(z <= 0) e_part_occ->Fill(x, y); // electron part xy occupancy (Z < 0)

		// Fill occupancy by layer/vol_name
		th2_by_layer->Get(hit->vol_name)->Fill(x, y);

		// Fill occupancy per detector
		auto detector_name = VolNameToDetName(hit->vol_name);	// get detector name
		if(!detector_name.empty()) {
			th1_z_by_detector->Get(detector_name)->Fill(z);
			th2_by_detector->Get(detector_name)->Fill(x, y);
			th3_by_detector->Get(detector_name)->Fill(z, x, y);  // z, x, y is the right order here
			th3_hits3d->Fill(z, x, y);
		}
	}
}


//------------------
// Finish
//------------------
void OccupancyAnalysis::Finish()
{
	fmt::print("OccupancyAnalysis::Finish() called\n");

	// Next we want to create several pretty canvases (with histograms drawn on "same")
	// But we don't want those canvases to pop up. So we set root to batch mode
	// We will restore the mode afterwards
	bool save_is_batch = gROOT->IsBatch();
	gROOT->SetBatch(true);

	// 3D hits distribution
	auto th3_by_det_canvas = new TCanvas("th3_by_det_cnv", "Occupancy of detectors");
	dir_main->Append(th3_by_det_canvas);
	for (auto& kv : th3_by_detector->GetMap()) {
		auto th3_hist = kv.second;
		th3_hist->Draw("same");
	}
	th3_by_det_canvas->GetPad(0)->BuildLegend();

	// Hits Z by detector

	// Create pretty canvases
	auto z_by_det_canvas = new TCanvas("z_by_det_cnv", "Hit Z distribution by detector");
	dir_main->Append(z_by_det_canvas);
	th1_hits_z->Draw("PLC PFC");

	for (auto& kv : th1_z_by_detector->GetMap()) {
		auto hist = kv.second;
		hist->Draw("SAME PLC PFC");
		hist->SetFillStyle(3001);
	}
	z_by_det_canvas->GetPad(0)->BuildLegend();

	gROOT->SetBatch(save_is_batch);
}

std::string OccupancyAnalysis::VolNameToDetName(const std::string &vol_name)
{
	for(const auto &detector_name: detector_names){
		if(ej::StartsWith(vol_name, detector_name))
		{
			return detector_name;
		}
	}
	return std::string();
}

