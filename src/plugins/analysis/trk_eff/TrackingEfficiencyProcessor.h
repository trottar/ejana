
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>

#include <ejana/EServicePool.h>

#include <TDirectory.h>
#include <TH1D.h>

#include <io/g4e_reader/GeantPrimaryParticle.h>

class JApplication;

class TrackingEfficiencyProcessor : public JEventProcessor {
private:
    ej::EServicePool services_;  // TODO: Replace with JServiceLocator
    int verbose_ = 0;  // Output verbosity
    std::shared_ptr<JGlobalRootLock> m_lock;
    TDirectory *plugin_root_dir;   // Main TDirectory for Plugin histograms and data

    TH1D *h1d_pt_primaryparticle;
    TH1D *h1d_pt_true_all;
    TH1D *h1d_pt_true_reconstructible;
    TH1D *h1d_pt_reco;
    TH1D *h1d_pt_diff;
    TH1D *h1d_tracking_efficiency;
    TH1D *h1d_tracking_exceptions;

    std::set<int> pdgs_;  // All pdgs encountered so far

public:
    explicit TrackingEfficiencyProcessor(JApplication *app = nullptr) :
            JEventProcessor(app),
            services_(app) {};

    void Init() override;
    void Process(const std::shared_ptr<const JEvent> &event) override;
    void Finish() override;

};

