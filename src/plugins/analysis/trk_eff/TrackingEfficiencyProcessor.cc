#include "TRandom.h"
#include "TLorentzVector.h"
#include <TFile.h>

#include <fmt/core.h>

#include "TrackingEfficiencyProcessor.h"

#include <MinimalistModel/McTrack.h>
#include "RecoModel/RecoHit.h"
#include "RecoModel/RecoCluster.h"
#include "RecoModel/RecoTrack.h"

using namespace fmt;

void TrackingEfficiencyProcessor::Init()
{
    auto app = GetApplication();
    m_lock = app->GetService<JGlobalRootLock>();
    auto pm = app->GetService<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    verbose_ = 0;
    pm->SetDefaultParameter("trk_eff:verbose", verbose_, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // Setup histograms. We get current TFile from Service Locator
    auto file = services_.Get<TFile>();


    m_lock->acquire_write_lock();

    // create a subdirectory "hist_dir" in this file
    plugin_root_dir = file->mkdir("trk_eff");
    file->cd();         // Just in case some other root file is the main TDirectory now

    h1d_pt_primaryparticle = new TH1D("pt_primary_particle", "tranverse p", 100,0,10);
    h1d_pt_primaryparticle->SetDirectory(plugin_root_dir);

    h1d_pt_true_all = new TH1D("pt_true_all", "tranverse p", 100,0,10);
    h1d_pt_true_all->SetDirectory(plugin_root_dir);

    h1d_pt_true_reconstructible = new TH1D("pt_true_reconstructible", "tranverse p", 100,0,10);
    h1d_pt_true_reconstructible->SetDirectory(plugin_root_dir);

    h1d_pt_reco = new TH1D("pt_reco", "reco p", 100,0,10);
    h1d_pt_reco->SetDirectory(plugin_root_dir);

    h1d_pt_diff = new TH1D("pt_diff", "diff p", 100,-10,10);
    h1d_pt_diff->SetDirectory(plugin_root_dir);

    h1d_tracking_efficiency = new TH1D("tracking_efficiency", "successful fits/reconstructible fits", 100, 0, 1);
    h1d_tracking_efficiency->SetDirectory(plugin_root_dir);

    h1d_tracking_exceptions = new TH1D("tracking_exceptions", "excepting fits/reconstructible fits", 100, 0, 1);
    h1d_tracking_exceptions->SetDirectory(plugin_root_dir);

    // TODO: Do we want these?
    /*hD0_mass = new TH1D("D0_mass", "D0 mass", 100,1.45,2.2);
    hD0_mass->GetXaxis()->SetTitle("m_{K#pi} [GeV/c^{2}]");
    hD0_mass->SetDirectory(plugin_root_dir);

    hD0_vtx= new TH1D("D0_vtx", "D0 vtx", 100,0.,300.);
    hD0_vtx->GetXaxis()->SetTitle("vertex [#mum]");
    hD0_vtx->SetDirectory(plugin_root_dir);

    hD0_pt = new TH1D("D0_pt", "D0 pt", 100,0.,5.);
    hD0_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    hD0_pt->SetDirectory(plugin_root_dir);*/

    m_lock->release_lock();
}


void TrackingEfficiencyProcessor::Process(const std::shared_ptr<const JEvent> &event)
{

    auto hits = event->Get<RecoHit>();
    auto clusters = event->Get<RecoCluster>();
    auto reco_tracks = event->Get<RecoTrack>();
    auto mc_tracks = event->Get<minimodel::McTrack>();
    auto primary_particles = event->Get<jleic::GeantPrimaryParticle>(); // TODO: What if these don't exist?

    m_lock->acquire_write_lock();

    if (verbose_) {
        // We want all of this inside the lock so that all per-event data shows up together
        print("\n====================================\n");
        print("trk_eff::Process( event number = {} ) \n", event->GetEventNumber());

        for (auto hit : hits) {
            print("Hit: track {}: {}, {}, {}\n", hit->track_id, hit->Xpos, hit->Ypos, hit->Zpos);
        }
        for (auto track : reco_tracks) {
            print("Genfit: track {}: {}, {}, {}\n", track->track_id, track->x.x(), track->x.y(), track->x.z());
        }
        for (auto track : mc_tracks) {
            print("Geant: track {}: {}, {}, {}\n", track->id, track->vtx_x, track->vtx_y, track->vtx_z);
        }
        for(auto particle: primary_particles) {
            print("Geant: primaryparticle {}, {:<10} \n", particle->id, particle->pdg);
        }
    }

    // Process raw Geant4 events if available
    uint64_t primary_particle_count = 0;
    for(auto particle: primary_particles) {
        pdgs_.insert(particle->pdg);
        double pt_true = particle->tot_mom*sqrt(particle->dir_x*particle->dir_x + particle->dir_y * particle->dir_y);
        h1d_pt_primaryparticle->Fill(pt_true);
        primary_particle_count++;
    }

    std::map<uint64_t, const minimodel::McTrack*> track_lookup;
    for (auto mc_track : mc_tracks) {
        assert(track_lookup.find(mc_track->id) == track_lookup.end());
        track_lookup[mc_track->id] = mc_track;

        auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);
        h1d_pt_true_all->Fill(mc_pt);
    }

    for(auto reco_track : reco_tracks) {

        auto mc_track = track_lookup[reco_track->track_id];
        auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);

        h1d_pt_reco->Fill(reco_track->p.Pt());
        h1d_pt_true_reconstructible->Fill(mc_pt);
        h1d_pt_diff->Fill(reco_track->p.Pt() - mc_pt);
    }

    auto track_metadata = event->GetMetadata<RecoTrack>();
    double eff = (1.0 * track_metadata.succeeded_count) /
                 (track_metadata.total_track_count - track_metadata.insufficient_data_count);

    h1d_tracking_efficiency->Fill(eff);

    double exc = (1.0 * track_metadata.exception_count) /
                 (track_metadata.total_track_count - track_metadata.insufficient_data_count);

    h1d_tracking_exceptions->Fill(exc);

    m_lock->release_lock();
}


void TrackingEfficiencyProcessor::Finish()
{
    fmt::print("Found pdgs: \n");
    for (int pdg: pdgs_) {
        fmt::print("{}, ", pdg);
    }
    fmt::print("\n");
}

