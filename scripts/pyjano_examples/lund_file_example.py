from pyjano.jana import Jana

jana = Jana()
jana.exec_path='/home/romanov/eic/ejana/dev/cmake-build-debug/src/programs/ejana/ejana'
jana.plugin_search_paths=['/home/romanov/eic/ejana/dev/cmake-build-debug']
jana.plugin('lund_reader') \
    .plugin('jana', nevents=10000, output='mydiHadSmearedOut.root') \
    .plugin('eic_smear') \
    .plugin('event_writer') \
    .plugin('vmeson')\
    .source('/home/romanov/Downloads/pipi.lund')\
    .run()


