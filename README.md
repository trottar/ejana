![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/eic/ejana/master)


Quick start
===========

The quickest start is to use docker containers loaded with eJana

Running with X11
----------------

First run the docker container:

```bash
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --rm -it --user $(id -u) eicdev/ejana-centos7:latest
```

Then run EJANA in it:
```bash
ej_root -Pplugins=jleic_geant_reader,vmeson,jleic_occupancy -Pnthreads=1 -PRoot:StartGui=1 /home/eicuser/ejana_sample_data/herwig7_dis_3k.root
```

What should happend? - you should see events being processed and then Root TBrowser will open with the input file and the result:

```
... 
Final Report
------------------------------------------------------------------------------
Source                                              Nevents  Queue   NTasks
------------------------------------------------------------------------------
/home/eicuser/ejana_sample_data/g4e_output.root        4000  Events  4000
```

What just happened? 

- ejana - stands for EJANA root. Is a ejena entry point that can deal with CERN.ROOT. 
`jana` executable is ROOT agnostic and doesn't depend on CERN.ROOT by itself. 
But most of the plugin at this point needs ROOT output and infrastructure. 
```ejana``` provides it.

- ```-Pplugins``` run plugins:
    * jleic_geant_reader - knows how to open g4e Geant4 output files
    * vmeson - some sample analysis
    * jleic_occupancy - some occupancy diagnostic plots
- ```-Pnthreads=1``` how many threads to run

- ```-PRoot:StartGui=1``` ask ejana gui infrastructure to open TBrowser in the end (more versatile DQM style gui is upcoming)
- ```/home/.../g4e_output.root``` some input file
    

 

Download and build software
============================

ejpm 
----

**ejpm** stands for **e**<sup>**J**ANA</sup> **p**acket ~~**m**anager~~ helper

**The main goal** of ejpm is to provide easy experience of:

* installing e<sup>JANA</sup> reconstruction framework and dependent packages
* unify installation for different environments: various operating systems, docker images, etc. 

The secondary goal is to help users with e^JANA plugin development cycle.

ejpm on pipy:
https://pypi.org/project/ejpm/

ejpm on gitlab:
https://gitlab.com/eic/ejpm


<br>

Manual installation
-------------------

2. **ejpm is not a requirment** for e<sup>JANA</sup>. It is not a part of e<sup>JANA</sup> 
    build system and one can compile and install e<sup>JANA</sup> without ejpm. But... 
    
At this point e<sup>JANA</sup> depends on:

- CERN ROOT
- Genfit
- Rave
- JANA2

With them installed one can download and build eJana. 


